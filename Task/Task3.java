package Task;

public class Task3 {

    public static void main(String[] args) {
        booleanExpression(1.0D, 0.5D);
    }

    public static void booleanExpression(double x, double y) {
        boolean result;
        if ((x < 0.0D || y < 1.5D * x - 1.0D || y > x) && (x > 0.0D || y < -1.5D * x - 1.0D || y > -x)) {
            result = false;
        } else {
            result = true;
        }

        System.out.print(" Результат расположения точки  в диапазонах   = " + result);
    }
}
