package Task;

public class Task1 {

    public static void main(String[] args) {
        shotCalculate(45.0D, 20.0D);
    }

    public static void shotCalculate(double a, double v) {
        double elevationAngle = 0.0D;
        double startingSpeed = 0.0D;
        double accelerationOfGravity = 9.80665D;
        double flightDistance = Math.pow(v, 2.0D) / accelerationOfGravity * Math.sin(Math.toRadians(2.0D * a));
        System.out.print("Растояние полета снаряда \t" + flightDistance);
    }
}
