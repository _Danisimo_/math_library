package Task;

public class Task4 {

    public static void main(String[] args) {
        expressionCalculate(10.0D);
    }

    public static void expressionCalculate(double x) {
        double result = 6.0D * Math.log(Math.sqrt(Math.exp(x + 1.0D) + 2.0D * Math.exp(x) * Math.cos(x))) / Math.log(x - Math.exp(x + 3.0D) * Math.sin(x)) + Math.abs(Math.cos(x) / Math.exp(Math.sin(x)));
        System.out.print(" Результат выражения    = " + result);
    }
}
